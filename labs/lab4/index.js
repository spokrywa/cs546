const animals = require("./data/animals");

const main = async () => {
    const sasha = await animals.create("Sashsa", "Dog");
    console.log(sasha);
    const lucy = await animals.create("Lucy", "Dog");
    console.log(lucy);
    console.log(await animals.getAll());
    const duke = await animals.create("Duke", "Walrus");
    console.log(duke);
    console.log(await animals.rename(sasha._id, "Sashita"));
    await animals.remove(lucy._id);
    console.log(await animals.getAll());
}

main();