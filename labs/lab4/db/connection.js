const mongo = require("mongodb").MongoClient;
const settings = require("./settings");
const mongocfg = settings.mongocfg;

let con = undefined;
let db = undefined;

module.exports = async () => {
  if (!con) {
    con = await mongo.connect(mongocfg.serverUrl);
    db = await con.db(mongocfg.database);
  }
   return db;
}