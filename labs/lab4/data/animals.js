const collections = require("../db/collections");
const mongo = require("mongodb");
const animals = collections.animals;

function checkId(id){
    if(!id)
        throw "An ID has to be provided";
    else if(!mongo.ObjectID.isValid(id))
        throw "Provided animal ID is not valid";
    return id;
}

module.exports = {

    async get(id){
        checkId(id);
        
        const animalcoll = await animals();
        const animal = await animalcoll.findOne({_id:id});

        if(animal == null)
            throw "Animal not found in database";

        return animal;
    },

    async create(name, animalType){
        if(!name)
            throw "Need to provide name for animal";
        else if(typeof name != 'string')
            throw "Name provided needs to be a valid string";
        if(!animalType)
            throw "Need to provide a type for the animal";
        else if(typeof animalType != 'string')
            throw "Animal type provided needs to be a string";

        const animalcoll = await animals();

        const newAnimal = {
            name: name,
            animalType:animalType
        };

        const insert = await animalcoll.insertOne(newAnimal);
        if(insert.insertedCount === 0)
            throw "Could not add animal";

        const id = insert.insertedId;
        return await this.get(id);
    },

    async getAll(){
        const animalcoll = await animals();
        const arrayanimals = await animalcoll.find({}).toArray();
        return arrayanimals;
    },

    async remove(id){
        checkId(id);
        const animal = await this.get(id);
        const animalcoll = await animals();
        const deleteinf = await animalcoll.removeOne({_id:id});
        if(deleteinf.deletedCount === 0)
            throw "Animal could not be deleted";
        return animal;
    },

    async rename(id, newName){
        checkId(id);
        if(!newName)
            throw "A new name must be provided";
        else if(typeof newName != 'string')
            throw "Name provided is not a valid string";
        
        const animalcoll = await animals();

        const update = await animalcoll.updateOne(
            {_id:id}, 
            {$set: {name:newName}});
        if(update.modifiedCount === 0)
            throw "Animal's name could not be updated";
        
        return await this.get(id);
    }


}