const express = require("express");
const axios = require("axios");
const router = express.Router();

router.post("/", async (req, res) => {
    let input = req.body.personName;
    if(!input){
        res.status(400).render("search", {
            title:"People Finder",
            error: "Error! Search input was blank"
        });
        return;
    }
    try{
        const  {data} = await axios.get('https://gist.githubusercontent.com/robherley/5112d73f5c69a632ef3ae9b7b3073f78/raw/24a7e1453e65a26a8aa12cd0fb266ed9679816aa/people.json');
        let people = data.filter(x => x.firstName.toLowerCase().includes(input.toLowerCase()) || x.lastName.toLowerCase().includes(input.toLowerCase()));
        if (people.length > 20)
            people = people.slice(0, 20);
        res.render("found", {title:"People Found", people: people, personName:input}); 
    }catch(e){
        res.sendStatus(500);
        console.log(e);
    }
});

module.exports = router;