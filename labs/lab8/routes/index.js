const searchRoute = require("./search");
const detailRoute = require("./details");

const constructorMethod = app => {
    app.use("/search", searchRoute);
    app.use("/details", detailRoute);
    app.get("/", (req,res) =>{
        res.render("search", {title:"People Finder"});
    })
    app.use("*", (req, res) => {
        res.sendStatus(404);
    })
};

module.exports = constructorMethod;