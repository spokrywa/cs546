const express = require("express");
const axios = require("axios");
const router = express.Router();

router.get("/:id", async (req, res) => {
    const {data} = await axios.get('https://gist.githubusercontent.com/robherley/5112d73f5c69a632ef3ae9b7b3073f78/raw/24a7e1453e65a26a8aa12cd0fb266ed9679816aa/people.json');
    let person = data.find(x => x.id == req.params.id);
    res.render("details",
    {
        title:"Person Found",
        id:person.id,
        firstName:person.firstName,
        lastName:person.lastName,
        address:person.address,
        zip:person.zip,
        phone:person.phone,
        ssn:person.ssn
    });
});

module.exports = router;