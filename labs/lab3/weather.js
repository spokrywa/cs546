const people = require("./people.js");
const axios = require("axios");

async function getWeather(){
    const { data } = await axios.get('https://gist.githubusercontent.com/robherley/1b950dc4fbe9d5209de4a0be7d503801/raw/eee79bf85970b8b2b80771a66182aa488f1d7f29/weather.json');
    return data;
}


async function shouldTheyGoOutside(firstName, lastName){
    if(firstName == undefined || lastName == undefined){
        throw "Not enough inputs given";
    }
    if(typeof firstName != "string" || typeof lastName != "string"){
        throw "Inputs are not valid strings";
    }

    let pdata = await people.getPeople();
    let wdata = await getWeather();
    let zipcode = 0;
    for( let i = 0; i < pdata.length; i++){
        if(pdata[i]["firstName"] == firstName && pdata[i]["lastName"] == lastName){
            zipcode = pdata[i]["zip"];
            break;
        }
    }if(zipcode = 0){
        throw "Person not found in data."
    }

    for(let i = 0; i < wdata.length; i++){
        if(wdata[i]["zip"] == zipcode){
            if(wdata[i]["temp"] >= 34){
                return `Yes, ${firstName} should go outside.`
            }else{
                return `No, ${firstName} should not go outside.`
            }
        }
    }
}

module.exports = {
    shouldTheyGoOutside
}