const axios = require("axios");
const people = require("./people");


async function getWork(){
    const { data } = await axios.get('https://gist.githubusercontent.com/robherley/61d560338443ba2a01cde3ad0cac6492/raw/8ea1be9d6adebd4bfd6cf4cc6b02ad8c5b1ca751/work.json');
    return data;
}

async function whereDoTheyWork(firstName, lastName){
    if(firstName == undefined || lastName == undefined){
        throw "Not enough inputs given";
    }
    if(typeof firstName != "string" || typeof lastName != "string"){
        throw "Inputs are not valid strings";
    }

    let pdata = await people.getPeople();
    let wdata = await getWork();
    let ssn = "";
    for( let i = 0; i < pdata.length; i++){
        if(pdata[i]["firstName"] == firstName && pdata[i]["lastName"] == lastName){
            ssn = pdata[i]["ssn"];
            break;
        }   
    }
    if(ssn === ""){
        throw "Person not found";
    }

    

    for(let i = 0; i < wdata.length; i++){
        if(wdata[i]["ssn"] == ssn){
            let baseString = firstName + " " + lastName + " - " + wdata[i]["jobTitle"] + " at " + wdata[i]["company"] + ". ";
            if(wdata[i]["willBeFired"]){
                return baseString + "They will be fired.";
            }
            else{
                return baseString + "They will not be fired.";
            }
        }
    }

}

async function findTheHacker(ip){
    if(ip == undefined){
        throw "No input given";
    }
    //took regex from stackoverflow
    if (!/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {  
        throw "Not a valid IP address"
    }

    let pdata = await people.getPeople();
    let wdata = await getWork();
    let ssn = "";

    for(let i = 0; i < wdata.length; i++){
        if(wdata[i]["ip"] == ip){
            ssn = wdata[i]["ssn"]
        }
    }if(ssn === ""){
        throw "IP address not found";
    }

    for( let i = 0; i < pdata.length; i++){
        if(pdata[i]["ssn"] == ssn){
            let firstName = pdata[i]["firstName"];
            let lastName = pdata[i]["lastName"];
            return `${firstName} ${lastName} is the hacker!`
        }   
    }
}


module.exports = {
    whereDoTheyWork,
    findTheHacker
}