const people = require("./people.js");
const weather = require("./weather.js");
const work = require("./work.js");

async function main(){
    try{
        console.log(await people.firstNameMetrics());
        console.log(await people.lexIndex(4));
        console.log(await weather.shouldTheyGoOutside("Hank", "Tarling"));
    }catch(e){
        console.log(e);
    }

    try{
        console.log(await work.whereDoTheyWork("Hank", "Tarling"));
        console.log(await work.whereDoTheyWork("Hank"));
        console.log(await work.findTheHacker("79.222.167.180"));
        console.log(await work.findTheHacker("test"));
    }catch(e){
        console.log(e);
    }
}


main();