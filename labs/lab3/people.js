const axios = require("axios");

function checkInput(num){
    if(num == undefined){
        throw "Input not given or undefined";
    }else if(num < 0){
        throw "Input cannot be negative"
    }else if(!Number.isInteger(num)){
        throw "Input not an integer";
    }else{
        return num;
    }
}

function sortKey(arr, key){
    return arr.sort(function(a,b) {
        let x = a[key];
        let y = b[key];
        return ( (x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

async function getPeople(){
    const { data } = await axios.get('https://gist.githubusercontent.com/robherley/5112d73f5c69a632ef3ae9b7b3073f78/raw/24a7e1453e65a26a8aa12cd0fb266ed9679816aa/people.json');
    return data;
}

async function getPersonById(id){
    checkInput(id);
    let data = await getPeople();
    for(let i = 0; i < data.length; i++){
        if(data[i]["id"] == id){
            return data[i];
        }
    }
}

async function lexIndex(index){
    checkInput(index);
    let data = await getPeople();
    sortKey(data, "lastName");
    return data[index]["firstName"] + " " + data[index]["lastName"];
}

async function firstNameMetrics(){
    let data = await getPeople();
    let metrics = {
        totalLetters: 0,
        totalVowels: 0,
        totalConsonants: 0,
        longestName: 0,
        shortestName: 0
    }
    metrics["longestName"] = data[0]["firstName"];
    metrics["shortestName"] = data[0]["firstName"];
    for(let i = 0; i < data.length; i++){
        let str = data[i]["firstName"];
        metrics["totalLetters"]+=str.length;

        let vmatch = str.match(/[aeiou]/gi);
        if(vmatch != null){
            metrics["totalVowels"]+= vmatch.length;
            metrics["totalConsonants"]+= str.length - vmatch.length;
        }else{
            metrics["totalConsonants"]+= str.length;
        }

        if(str.length < metrics["shortestName"].length){
            metrics["shortestName"] = str;
        }
        if(str.length > metrics["longestName"].length){
            metrics["longestName"] = str;
        }
    }
    return metrics;
}

module.exports = {
    getPeople,
    getPersonById,
    lexIndex,
    firstNameMetrics
}