const questionOne = function questionOne(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        if(typeof arr[i] == 'number'){
            sum += arr[i] * arr[i];
        }else{
            return NaN;
        }
    }
    return sum;
}

const questionTwo = function questionTwo(num) { 
    if (num < 0) {
        return 0;
    }else if(num == 1){
        return 1;
    }else{
        return questionTwo(num-1) + questionTwo(num-2);
    }
}

const questionThree = function questionThree(text) {
    let matches = text.match(/[aeiou]/gi);
    return matches.length;
}

const questionFour = function questionFour(num) {
    if(num < 0){
        return NaN;
    }else if(num == 0 || num == 1){
        return 1;
    }else{
        return num * questionFour(num-1);
    }
}

module.exports = {
    firstName: "Sebastian", 
    lastName: "Pokrywa", 
    studentId: "10408740",
    questionOne,
    questionTwo,
    questionThree,
    questionFour
};