function validate(){
    let x = document.forms["primeChecker"]["number"].value;
    console.log(x);
    if(!x){
        alert("Input was not given");
        return false;
    }
    console.log("validated");
    if(primeCheck(x)){
        let temp = document.createElement("P");
        let text = document.createTextNode(x + " is a prime number");
        temp.appendChild(text);
        temp.className = "is-prime";
        document.getElementById("attempts").appendChild(temp);
    }else{
        let temp = document.createElement("P");
        let text = document.createTextNode(x + " is a NOT prime number");
        temp.appendChild(text);
        temp.className = "not-prime";
        document.getElementById("attempts").appendChild(temp);
    }
    //prevents page reload to return false so list doesnt get cleared
    return false;
}

function primeCheck(num){
    if(num < 0)
        return false;
    for(let i = 2; i < num; i++){
        if(num % i == 0)
            return false;
    }
    return true;
}