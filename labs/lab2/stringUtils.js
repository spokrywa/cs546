function checkInput(string){
    if(typeof string == 'string' && string != undefined){
        return string;
    }else{
        throw "ERROR: Input not a string";
    }
}

function capitalize(string){
    checkInput(string);
    return string.charAt(0).toUpperCase() + string.substr(1).toLowerCase();
}

function repeat(string, num){
    checkInput(string);
    if(typeof num != 'number' || num < 0){
        throw "ERROR: Number provided not valid";
    }
    if(num == 0){
        return "";
    }else{
        let newstr = "";
        for(let i = 0; i < num; i++){
            newstr += string;
        }
        return newstr;
    }
}

function countChars(string){
    checkInput(string);
    let elements = {};
    if(string.length == 0){
        return elements;
    }else{
        for(let i = 0; i < string.length; i++){
            if(elements[string[i]] == undefined){
                elements[string[i]] = 1;
            }else{
                elements[string[i]]++;
            }
        }
        return elements;
    }
}


module.exports = {
    capitalize,
    repeat,
    countChars
}