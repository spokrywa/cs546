function checkInput(obj){
    if(typeof obj != 'object'){
        throw "ERROR: Input not an object"
    }
}

function extend(...args){
    if(args.length < 2){
        throw "ERROR: Not enough arguments";
    }

    for(let i = 0; i < args.length; i++){
        checkInput(args[i]);
    }

    let newobj = {};

    for(let i = 0; i < args.length; i++){
        for(key in args[i]){
            if(newobj[key] == undefined){
                newobj[key] = args[i][key];
            }
        }
    }

    return newobj;
}

function smush(...args){
    if(args.length < 2){
        throw "ERROR: Not enough arguments";
    }

    for(let i = 0; i < args.length; i++){
        checkInput(args[i]);
    }

    let newobj = {};

    for(let i = 0; i < args.length; i++){
        for(key in args[i]){
            newobj[key] = args[i][key];
        }
    }

    return newobj;
}

function mapValues(object, func){
    checkInput(object);
    if(typeof func != 'function' || func == undefined){
        throw "ERROR: No valid function provided";
    }

    let newobj = {}
    for(let key in object){
        newobj[key] = func(object[key]);
    }
    return newobj;
}


module.exports = {
    extend,
    smush,
    mapValues
}