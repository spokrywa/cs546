
function checkInput(array){
    if(Array.isArray(array)){
        return array;
    }
    else{
        throw "ERROR: Input not an array";
    }
}

function head(array){
    checkInput(array);
    if(array.length > 0){
        return array[0];
    }else{
        throw "ERROR: Empty array";
    }
}

function last(array){
    checkInput(array);
    if(array.length > 0){
        return array[array.length - 1];
    }else{
        throw "ERROR: Empty array";
    }
}

function remove(array, index){
    checkInput(array);
    if(index > array.length-1 || index < 0){
        throw "ERROR: Index out of bounds";
    }else if(array.length == 0){
        throw "ERROR: Empty array"
    }else{
        array.splice(index, 1);
        return array;
    }
}

function range(end, value){
    if(end < 0 || !Number.isInteger(end)){
        throw "ERROR: End value not valid"
    }else{
        let array = [];
        for(let i = 0; i < end; i++){
            if (value != undefined){
                array.push(value);
            }else{
                array.push(i);
            }
        }
        return array;
    }
}

function countElements(array){
    checkInput(array);
    let elements = {};
    if(array.length == 0){
        return elements;
    }else{
        for(let i = 0; i < array.length; i++){
            if(elements[array[i]] == undefined){
                elements[array[i]] = 1;
            }else{
                elements[array[i]]++;
            }
        }
        return elements;
    }
}


function isEqual(arrayOne, arrayTwo){
    if (arrayOne.length != arrayTwo.length){
        return false;
    }else if(arrayOne.length == 0 && arrayTwo.length == 0){
        return true;
    }
    for(let i = 0; i < arrayOne.length; i++){
        if(arrayOne[i] != arrayTwo[i] || typeof arrayOne[i] != typeof arrayTwo[i]){
            return false;
        }
    }
    return true;
}

module.exports = {
    head,
    last,
    remove,
    range,
    countElements,
    isEqual
}