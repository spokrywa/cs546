const arrayUtils = require("./arrayUtils.js");
const stringUtils = require("./stringUtils.js");
const objUtils = require("./objUtils.js");

try{
    console.log(arrayUtils.range(3, "turtle"));
}catch(e){
    console.log(e);
}try{
    console.log(arrayUtils.range(-3));
}catch(e){
    console.log(e);
}

try{
    console.log(stringUtils.capitalize("tesTiNgStrInG"));
}catch(e){
    console.log(e);
}try{
    console.log(stringUtils.capitalize([3,2,3]));
}catch(e){
    console.log(e);
}

try{
    console.log(stringUtils.countChars("test string, for testing purposes"));
}catch(e){
    console.log(e);
}try{
    console.log(stringUtils.countChars(4));
}catch(e){
    console.log(e);
}

try{
    console.log(objUtils.mapValues({ a: 1, b: 2, c: 3 }, n => n + 1));
}catch(e){
    console.log(e);
}try{
    console.log(objUtils.mapValues({ a: 1, b: 2, c: 3 }, 17));
}catch(e){
    console.log(e);
}
