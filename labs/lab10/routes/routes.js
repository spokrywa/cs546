const express = require("express");
const bcrypt = require("bcrypt");
const users = require("../data/users.js")
const router = express.Router();

router.post('/login', async (req, res) => {
    let user = users.find(u => u.username == req.body.username);
    if(!user){
        res.status(401).render('loginpage', {error: true});
        return;
    }

    const auth = await bcrypt.compare(req.body.password, user.hashedPassword);
    if(auth){
        delete user.hashedPassword;
        req.session.user = user;
        res.render('private', {user: user})
    }else{
        res.status(401).render('loginpage', {error: true});
    }
});

router.get('/private', async (req, res) => {
    res.render('private', {user: req.session.user});
});

router.get('/logout', async (req, res) => {
    req.session.destroy();
    res.render('logout');
});

module.exports = router;