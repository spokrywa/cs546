const router = require("./routes");
const session = require('express-session');

const constructorMethod = app => {
    app.use(session({
        name: 'AuthCookie',
        secret: 'secret string',
        resave: false,
        saveUninitialized: true,
    }));

    //logging middleware
    app.use((req, res, next) =>{
        let time = new Date().toUTCString();
        if(req.session.user)
            console.log(time + ": " +req.method + " " + req.originalUrl + " (Authenticated User)");
        else
            console.log(time + ": " +req.method + " " + req.originalUrl + " (Unauthenticated User)");
        next();
    });

    //auth middleware
    app.use('/private', (req, res, next) => {
        if(!req.session.user){
            res.status(403).render('errorpage');
            return;
        }
        next();
    });

    app.get("/", (req, res) => {
        if(req.session.user)
            res.render('private', {user: req.session.user});
        else
            res.render('loginpage');
    })

    app.use("/", router);
    
};

module.exports = constructorMethod;