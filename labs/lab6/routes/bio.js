const express = require("express");
const router = express.Router();


router.get("/about", async (req, res) => {
    try {
      let getAbout = {
        "name": "Sebastian Pokrywa",
        "cwid": "10408740",
        "biography": "Born on the sacrifical altar of an ancient Mayan temple, Sebastian was raised in the jungles by ferocious pack of jaguars. Through years of struggle and hardship he became a fierce warrior and enlisted in the Royal Aztec Marine Corps on the brink of WW3. Sebastian spent his young adult life fighting in dense rainforests and harsh swamps throughout South East Asia moving from shore to shore laying waste to all of His Majesty's enemies. \nAfter the war, Sebastian gave up the rough and sunny lifestyle and moved to a remote location in Alaska where he spent the rest of his life raising sled dogs to ferry supplies from village to village.",
        "favoriteShows": ["M*A*S*H S2", "M*A*S*H S3", "M*A*S*H S4", "M*A*S*H S1"],
        "hobbies": ["Underwater Basket Weaving", "Swimming in the Arctic", "Knife Fighting"]
      }
      res.json(getAbout);
    } catch (e) {
      res.status(500).send();
    }
  });

router.get("/story", async (req, res) => {
    try {
      let getStory = {
        "storyTitle": "Covert Operation",
        "story": "I made my way into the base through an underwater diving operation using state of the art scuba gear. My mission? To eradicate a new nuclear weapon being developed by a rogue agency of the US government in a classified location. Details were sparse and I was only armed with a single .45 caliber USP, a few rations, and my sneaking suit. Resistance was heavy but through years of training and advanced knowledge of CQC I managed to dispose of or evade all the guards along the way.\n Eventually I was situated in a state of the art research and weapons lab. My current objective was to interrogate one of the scientists working on the project to find out more details about this new nuclear capable weapon. A problem occured however, as a cyborg ninja who only identified himself by the name Grey Fox went rogue himself. He wrecked havoc on the guards and made the whole operation unstable. I was left with no choice but to confront him but with my masterful CQC technique I was able to dispose of him.\n Having dealt with the cyborg ninja, I questioned a scientist and found out highly classified information about the weapon that I am unable to share here. Nevertheless I was then tasked by command to make sure the weapon was not operational. After a few more hours of evading guards and collecting various key cards to progress it was finally possible to disable the weapon for good. To my surprise however the head of the base was none other than my twin brother, Liquid. Knowing me better than I knew myself I was finally found, and a battle ensued. Countless missiles, grenade and small arms fire led to a Pyrrhic victory of sorts. I managed to destroy the classified weapon in the ensuing combat however I was left with numerous injuries and worse yet my brother escaped.The operation was deemed a success at HQ but the battle was far from over. However that story is best saved for another time."
      }
      res.json(getStory);
    } catch (e) {
      res.status(500).send();
    }
  });

router.get("/education", async (req, res) => {
    try {
      let getEducation = [
        {
            "schoolName": "Mayan Marine Corps Academy",
            "degree": "Tongue Twisting",
            "favoriteClass": "Surviving Venemous Snakes 101",
            "favoriteMemory": "I once pulled a pit viper off my leg while I was getting beaten nearly unconscious by a local tribe for extra credit and used it as a weapon to make my escape"
        },
        {
            "schoolName": "Bolivian School of Guerilla Warfare",
            "degree": "Pastries",
            "favoriteClass": "Survival, Evasion, Resistance Escape 546",
            "favoriteMemory": "Managed to clear an entire bunker of opposing forces using only a grenade pin and a used dinner napkin"
        },
        {
            "schoolName": "Alaskan Institue of Arctic Conduct",
            "degree": "Dog Sledding",
            "favoriteClass": "Infiltration 304",
            "favoriteMemory": "Took down a Russian Hind D using a single Stinger missile and a chaff grenade."
        }
      ]
      res.json(getEducation);
    } catch (e) {
      res.status(500).send();
    }
  });


module.exports = router;