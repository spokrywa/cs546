const bioRoutes = require("./bio");

const constructorMethod = app => {
  app.use("/", bioRoutes);

  app.use("*", (req, res) => {
    res.status(404).json({ error: "Not found" });
  });
};

module.exports = constructorMethod;