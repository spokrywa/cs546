const mongo = require("mongodb").MongoClient;

let con = undefined;
let db = undefined;

mongocfg = {
    "serverUrl":"mongodb://localhost:27017",
    "database":"Sebastian_Pokrywa_Lab7"
}

module.exports = async () => {
  if (!con) {
    con = await mongo.connect(mongocfg.serverUrl);
    db = await con.db(mongocfg.database);
  }
   return db;
}