const collections = require("./collections");
const mongo = require("mongodb");
const animals = collections.animals;
const postFuncs = require("./posts")

function checkId(id){
    if(!id)
        throw "An ID has to be provided";
    else if(!mongo.ObjectID.isValid(id))
        throw "Provided ID is not valid";
    return id;
}

module.exports = {

    async get(id){
        checkId(id);
        const animalcoll = await animals();
        const animal = await animalcoll.find({_id:id});
        if(animal == null)
            throw "Animal not found in database";

        return animal;
    },

    async create(name, animalType){
        if(!name)
            throw "Need to provide name for animal";
        else if(typeof name != 'string')
            throw "Name provided needs to be a valid string";
        if(!animalType)
            throw "Need to provide a type for the animal";
        else if(typeof animalType != 'string')
            throw "Animal type provided needs to be a string";

        const animalcoll = await animals();

        const newAnimal = {
            name: name,
            animalType:animalType,
            likes:[]
        };

        const insert = await animalcoll.insertOne(newAnimal);
        if(insert.insertedCount === 0)
            throw "Could not add animal";

        const id = insert.insertedId;
        return await this.get(id);
    },

    async getAll(){
        const animalcoll = await animals();
        const arrayanimals = await animalcoll.find({}).toArray();
        return arrayanimals;
    },

    async remove(id){
        checkId(id);
        const animal = await this.get(id);
        const animalcoll = await animals();
        const deleteinf = await animalcoll.removeOne({_id:id});
        if(deleteinf.deletedCount === 0)
            throw "Animal could not be deleted";
        return animal;
    },

    async rename(id, newName){
        checkId(id);
        if(!newName)
            throw "A new name must be provided";
        else if(typeof newName != 'string')
            throw "Name provided is not a valid string";

        const animalcoll = await animals();

        const update = await animalcoll.updateOne(
            {_id:id},
            {$set: {name:newName}});
        if(update.modifiedCount === 0)
            throw "Animal's name could not be updated";

        return await this.get(id);
    },

    async retype(id, newType){
        checkId(id);
        if(!newType)
            throw "A new type must be provided";
        else if(typeof newType != 'string')
            throw "Type provided is not a valid string";

        const animalcoll = await animals();

        const update = await animalcoll.updateOne(
            {_id:id},
            {$set: {animalType:newType}});
        if(update.modifiedCount === 0)
            throw "Animal's type could not be updated";

        return await this.get(id);
    },

    async likes(animalId, postId, like){
        checkId(postId);
        checkId(animalId)
        if(!like)
            throw "Boolean needs to be provided for like/unlike";
        else if(typeof like != 'boolean')
            throw "Boolean provided is not valid";

        const animalcoll = await animals();

        if(like){
            const update = await animalcoll.updateOne(
                {_id:id},
                {$push: {likes: postId}}
            )
            if(update.modifiedCount === 0)
                throw "Like could not be registered";
        }else{
            const update = await animalcoll.updateOne(
                {_id:id},
                {$pull: {likes: postId}}
            )
            if(update.modifiedCount === 0)
                throw "Unlike could not be registered";
        }
    }
}