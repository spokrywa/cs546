const collections = require("./collections");
const mongo = require("mongodb");
const posts = collections.posts;

function checkId(id){
    if(!id)
        throw "An ID has to be provided";
    else if(!mongo.ObjectID.isValid(id))
        throw "Provided ID is not valid";
    return id;
}

module.exports = {

    async get(id){
        checkId(id);

        const postcoll = await posts();
        const post = await postcoll.find({_id:id});

        if(post == null)
            throw "Post not found in database";

        return post;
    },

    async create(title, author, content){
        if(!title)
            throw "Need to provide title";
        else if(typeof title != 'string')
            throw "Title provided needs to be a valid string";
        if(!author)
            throw "Need to provide an author for post";
        else if(typeof author != 'string' || !mongo.ObjectID.isValid(author))
            throw "Author provided needs to be a valid string or object ID";
        if(!content)
            throw "Need to provide content";
        else if(typeof content != 'string')
            throw "Content provided needs to be a valid string";

        const postcoll = await posts();

        const newPost = {
            title:title,
            author:author,
            content:content
        };

        const insert = await postcoll.insertOne(newPost);
        if(insert.insertedCount === 0)
            throw "Could not add post";

        const id = insert.insertedId;
        return await this.get(id);
    },

    async getAllbyId(id){
        const postcoll = await posts();
        console.log(id);
        const postarray = await postcoll.find({author:id}).toArray();
        console.log(postarray);
        return postarray;
    },

    async getAll(){
        const postcoll = await posts();
        const postarray = await postcoll.find({}).toArray();
        return postarray;
    },

    async remove(id){
        checkId(id);
        const post = await this.get(id);
        const postcoll = await posts();
        const deleteinf = await postcoll.removeOne({_id:id});
        if(deleteinf.deletedCount === 0)
            throw "Post could not be deleted";
        return post;
    },

    async removeall(authorid){
        checkId(id);
        const postcoll = await posts();
        postcoll.deleteMany({author:authorid});
    },

    async changeContent(id, newContent){
        checkId(id);
        if(!newContent)
            throw "New content must be provided";
        else if(typeof newContent != 'string')
            throw "Content provided is not a valid string";

        const postcoll = await posts();

        const update = await postcoll.updateOne(
            {_id:id},
            {$set: {content:newContent}});
        if(update.modifiedCount === 0)
            throw "Post's content could not be updated";

        return await this.get(id);
    },

    async changeTitle(id, newTitle){
        checkId(id);
        if(!newTitle)
            throw "New title must be provided";
        else if(typeof newTitle != 'string')
            throw "Title provided is not a valid string";

        const postcoll = await posts();

        const update = await postcoll.updateOne(
            {_id:id},
            {$set: {title:newTitle}});
        if(update.modifiedCount === 0)
            throw "Post's title could not be updated";

        return await this.get(id);
    }
}