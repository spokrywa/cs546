const express = require("express");
const router = express.Router();
const data = require("../data");
const animalData = data.animals;
const postData = data.posts;


router.get("/:id", async (req, res) => {
    try {
        let post = await postData.get(req.params.id);
        let animal = await animalData.get(post.author);
        post.author = {_id:post.author,name:animal.name};
    } catch (e) {
        res.status(404).json({ message: "Post not found" });
    }
});

router.get("/", async (req, res) => {
    try {
        let postList = await postData.getAll();
        for(let x of postList){
            let animal = await animalData.get(x.author);
            x.author = {_id:x.author, name:animal.name};
        }
        res.status(200).send(postList);
        return postList;
    } catch (e) {
        res.status(500).send();
    }
});

router.post("/", async (req, res) => {
    let post = req.body;
    if(!post.title || !post.content || !post.author)
        res.status(400).send();
    try{
        const newPost = await postData.create(post.title, post.author, post.content);
        res.status(200).send(newPost);
    }catch(e){
        res.status(500).send();
    }
});

router.put("/:id", async (req, res) => {
    try{
        let post = await postData.get(req.param.id);
    }catch(e){
        res.status(404).send();
    }
    let tmpPost= req.body;
    if(!tmpPost.title && !tmpPost.content)
        res.status(400).send();
    if(typeof tmpPost.title == 'string'){
        postData.changeTitle(req.param.id, tmpPost.title);
    }
    if(typeof tmpPost.content== 'string'){
        postData.changeContent(req.param.id, tmpPost.content);
    }
    res.status(200).send();
});

router.delete("/:id", async (req, res) => {
    try{
        let post = await postData.get(req.param.id);
    }catch(e){
        res.status(404).send();
    }
    let result = {
        "deleted":"true",
        "data":{}
    }
    let animal = await animalData.get(post.author);
    post.author = {_id:post.author, name:animal.name};
    result.data = post; 
    res.status(200).send(result);
});

module.exports = router;