const postRoutes = require("./posts");
const animalRoutes = require("./animals");

const constructorMethod = app => {
  app.use("/posts", postRoutes);
  app.use("/animals", animalRoutes);

  app.use("*", (req, res) => {
    res.status(404).json({ error: "Not found" });
  });
};

module.exports = constructorMethod;
