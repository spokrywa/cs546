const express = require("express");
const router = express.Router();
const data = require("../data");
const animalData = data.animals;
const postData = data.posts;


router.get("/:id", async (req, res) => {
    try {
        let animal = await animalData.get(req.params.id);
        let posts = await postData.getAllbyId(req.params.id);
        animal.posts = posts;
        res.json(animal);
        res.status(200).send();
    } catch (e) {
        res.status(404).json({ message: "Animal not found" });
    }
});

router.get("/", async (req, res) => {
    try {
        let animalList = await animalData.getAll();
        for(let x of animalList){
            let posts = await postData.getAllbyId(x.id);
            x.posts = posts;
        };
        res.status(200).send(animalList);
    } catch (e) {
        res.status(500).send();
    }
});

router.post("/", async (req, res) => {
    let animal = req.body;
    if(!animal.name || !animal.animalType)
        res.status(400 ).send();
    try{
        const newAnimal = await animalData.create(animal.name, animal.animalType);
        res.status(200).send(newAnimal);
    }catch(e){
        res.status(500).send();
    }
});

router.put("/:id", async (req, res) => {
    try{
        let animal = await animalData.get(req.param.id);
    }catch(e){
        res.status(404).send();
    }
    let tmpAnimal = req.body;
    if(!tmpAnimal.name && !tmpAnimal.animalType)
        res.status(400).send();
    if(typeof tmpAnimal.name == 'string'){
        animalData.rename(req.param.id, tmpAnimal.name);
    }
    if(typeof tmpAnimal.animalType == 'string'){
        animalData.retype(req.param.id, tmpAnimal.animalType);
    }
    res.status(200).send();
});

router.delete("/:id", async (req, res) => {
    try{
        let animal = await animalData.get(req.param.id);
    }catch(e){
        res.status(404).send();
    }
    let posts = await postData.getAllbyId(req.params.id);
    postData.removeall(req.param.id);
    animal.posts = posts;
    let result = {
        "deleted":"true",
        "data":{}
    }
    result.data = animal; 
    res.status(200).send(result);
});

module.exports = router;