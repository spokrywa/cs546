const dictionary = require("./dictionary.js");

try{
    console.log(dictionary.lookupDefinition("programming"));
}catch(e){
    console.log(e);
}

try{
    console.log(dictionary.lookupDefinition("testing"));
}catch(e){
    console.log(e);
}

try{
    console.log(dictionary.lookupDefinition(42));
}catch(e){
    console.log(e);
}

try{
    console.log(dictionary.lookupDefinition("sleuth"));
}catch(e){
    console.log(e);
}

try{
    console.log(dictionary.getWord("A sudden or irregular invasion or attack for war or spoils : raid"));
}catch (e){
    console.log(e);
}

try{
    console.log(dictionary.getWord(42));
}catch (e){
    console.log(e);
}

try{
    console.log(dictionary.getWord("Test definition should fail"));
}catch (e){
    console.log(e);
}

try{
    console.log(dictionary.getWord("to make an official decision about who is right in (a dispute) : to settle judicially"));
}catch (e){
    console.log(e);
}