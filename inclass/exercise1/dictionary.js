const words ={
    programming: "The action or process of writing computer programs.",
    charisma: "A personal magic of leadership arousing special popular loyalty or enthusiasm for a public figure (such as a political leader)",
    sleuth: "To act as a detective : search for information",
    foray: "A sudden or irregular invasion or attack for war or spoils : raid",
    adjudicate: "to make an official decision about who is right in (a dispute) : to settle judicially"
}

function checkInput(arg){
    if(typeof arg == 'string'){
        return arg;
    }
    else{
        throw "ERROR: Input not a string";
    }
}

function lookupDefinition(arg){
    checkInput(arg);
    if(words[arg] != undefined){
        return words[arg];
    }else{
        throw "ERROR: Word not found";
    }
}

function getWord(arg){
    checkInput(arg);
    let gword = Object.keys(words).find(key => words[key] === arg);
    if(gword != undefined){
        return gword;
    }else{
        throw "ERROR: Definition not found";
    }
}

module.exports = {
    lookupDefinition,
    getWord
};